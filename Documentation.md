# GDPR

## Introduction

For the European countries and for California, GDPR user consent is required to track users and data. 
To retrieve this consent, a popup is displayed when the user launches the application for the first time, asking him to fill a form and accepting data to be tracked.

## Integration Steps

1) **"Install"** or **"Upload"** FG GDPR plugin from the FunGames Integration Manager in Unity, or download it from here.

2) Click on the **"Prefabs and Settings"** button in the FunGames Integration Manager to fill up your scene with required components and create the Settings asset.